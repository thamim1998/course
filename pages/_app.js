import "../styles/globals.css";
import "rsuite/dist/styles/rsuite-default.css";
import Layout from "../src/Layout/CETLayout";
import store from "../src/store/configureStore";
import { Provider } from "react-redux";

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  );
}

export default MyApp;
