import Head from "next/head";
import styles from "../../styles/cart/cart.module.css";
import { Fragment, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  getCarts,
  getTotal,
  removeCart,
  totalAmount,
} from "../../src/store/counterSlice";
import { Icon } from "rsuite";

function cart() {
  const dispatch = useDispatch();
  const cartItem = useSelector(getCarts);
  const totalprice = useSelector(getTotal);

  useEffect(() => {
    dispatch({ type: totalAmount });
  });

  if (cartItem.length === 0) {
    return (
      <div className={styles.empty}>
        <h2> Cart is Empty </h2>
      </div>
    );
  }

  return (
    <Fragment>
      <Head>
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
          Crossorigin="anonymous"
        />
      </Head>
      <div className={styles.main}>
        <div className="container-xxl">
          <div className=" row">
            <div className="col-8">
              {cartItem.map((course) => {
                return (
                  <div className="card w-75" key={course.id}>
                    <div className="card-body">
                      <h5 className="card-title">{course.name}</h5>
                      <p className="card-text">"IBPS PO Exam"</p>
                      <p className="card-text">
                        Total Price:
                        <span>
                          <b> ₹{course.price} </b>
                        </span>
                      </p>
                      <button
                        href="#"
                        className="btn btn-danger"
                        onClick={() => dispatch(removeCart(course.id))}
                      >
                        Remove
                      </button>
                    </div>
                  </div>
                );
              })}
            </div>

            <div className="col-4">
              <div className={styles.offers}>
                <h4>
                  <Icon icon="credit-card"> </Icon> Coupon Code
                </h4>
                <span> View Offers </span>
              </div>
              <div className={styles.content}>
                <input type="text" placeholder="Enter Coupon Code" />
                <button> Apply </button>
                <p>
                  Every year lakhs of aspirants appear for bank exams, but very
                  handful of them clears it.{" "}
                  <a href="#"> Terms & Conditions </a>
                </p>{" "}
                <hr />
              </div>
              <div>
                <h4>
                  {" "}
                  <Icon icon="google-wallet"> </Icon> Wallet Balance
                </h4>
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    value=""
                    id="flexCheckChecked"
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexCheckChecked"
                  >
                    <b> Use Wallet Credits</b> <br />
                    You can use ₹200 (20% OFF of ₹1000) <br />
                    Check to avail this offer. This operation cannot be undone.
                    <a href="#"> Terms & Conditions </a>
                  </label>
                </div>
                <hr />
              </div>
              <div className={styles.totalAmount}>
                <h5> Amount to Pay: </h5>
                <p> <b> ₹{totalprice} </b>  </p>
                <button href="#" className="btn btn-danger">
                  Continue
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default cart;


//create page fetch from comp level
// fetch from pages level is static and server side props