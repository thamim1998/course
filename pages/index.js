import Banner from "../src/components/common/Banner";
import LowerPage from "../src/components/common/Lowerpage";
import Meganavbar from "../src/components/common/Meganavbar";
import Navbar from "../src/components/common/Navbar";
import Footer from "../src/components/common/Footer";

export default function Home() {
  return (
    <>
    <Navbar/>
      <Banner />
      <Meganavbar/>
      <LowerPage/>
    </>
  );
}
