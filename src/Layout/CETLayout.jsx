import Navbar from "../components/common/Navbar"
import Footer from "../components/common/Footer"

const Layout = ({children}) =>{
    return (
        <div> 
            <Navbar/>
            {children}
            <Footer/>
        </div>
    )
}

export default Layout