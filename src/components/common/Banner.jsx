import { Icon, Button } from "rsuite";
import styles from "../../../styles/components/Banner.module.css"


function Banner() {
  return (
    <div className= {styles.banner}>
      <img
        src="https://upgrad-images.s3-ap-southeast-1.amazonaws.com/images/banners/images_1619429326769_436%405x.jpg"
        alt="logo"
        className="bannerImg"
      />
      <div className={styles.topLeft}>
        <div> 
        <h2>#1 Trusted Brand for Banking Exam Preparation</h2>
        <p>
          {" "}
          Enrol in upGrad's online courses to gain certification in data
          science, digital marketing, product management, machine learning,
          software development, and more. Get the unique upGrad experience -
          learn through a content co-developed by academia and industry experts
          & get a dedicated mentor and career support.
        </p>
        <Button id="button" appearance="default"> LEARN MORE <span className="icon" > <Icon icon= "chevron-right" >  </Icon> </span> </Button>
      </div>
      </div>

    </div>
  );
}

export default Banner;