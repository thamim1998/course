import { Col, Grid, Icon, Row } from "rsuite";
import Link from "next/link";
import styles from "../../../styles/components/Footer.module.css";

function footer() {
  return (
    <footer className={styles.footerMain}>
      <Grid fluid className={styles.Grid}>
        <Row className="show-grid">
          <Col xs={24} sm={24} md={8} lg={12}>
            <h6 style={{ color: "white" }}> ABOUT </h6>
            <p>
              {" "}
              upGrad Jeet aligns with the brand’s ambition of Rural India
              penetration strategy in making <br /> Bharat employable. upGrad
              Jeet aims to ensure the win (“Jeet”) of every learner. Winners{" "}
              <br /> need to sweat it out every day to win and the course is
              exactly designed to make every <br />
              aspirant become an active learner. With daily and weekly goal
              settings, notification-based <br />
              study reminders, one-to-one mentorship, 24/7 doubt solving,
              revision modules, <br /> personalization all driven by technology,
              upGrad Jeet stands for participative learning. We <br /> have the
              best faculties to teach and make you solve thousands of questions
              while you <br /> learn from basic concepts to mix, and complex
              ones to ultimately solve questions at the <br /> actual exam level
              and beyond. The learning methodology is like a guided tour where
              each and every learner is supported according to their level.{" "}
            </p>
          </Col>
          <Col xs={24} sm={24} md={8} lg={6}>
            <ul className="middle">
              <h6 style={{ color: "white" }}> ONLINE COACHING </h6>
              <li>
                {" "}
                <Link href="/google.com"> </Link>{" "}
                <a> Coaching for IBPS PO Exam</a>
              </li>
              <li>
                {" "}
                <Link href="/google.com"> </Link>{" "}
                <a> Coaching for SSC CGL Exam</a>
              </li>
              <li>
                {" "}
                <Link href="/google.com"> </Link>{" "}
                <a> Coaching for SSC CHSL Exam</a>
              </li>
              <li>
                {" "}
                <Link href="/google.com"> </Link>{" "}
                <a> Coaching for NRA CET Exam</a>
              </li>
            </ul>
          </Col>
          <Col xs={24} sm={24} md={8} lg={6}>
            <h6 style={{ color: "white" }}> COMPANY </h6>
            <p>
              {" "}
              THE GATE ACADEMY (HO)
              <br /> 3rd Floor, Novel Tech Park, #46/4,
              <br />
              Kudlu Gate, Hosur Rd,
              <br /> HSR Extension, Bengaluru,
              <br /> Karnataka 560068
              <br /> Ph: +91 8040 611 000
            </p>
          </Col>
        </Row>
        <hr />
        <Row className="show-grid">
          <Col xs={12}>
            <p> Copyright © 2021 All Rights Reserved by UpGrad Jeet. </p>
          </Col>
          <Col xs={12} className={styles.icon}>
            <Icon icon="facebook-official" size="3x">
              {" "}
            </Icon>
            <Icon icon="twitter" size="3x">
              {" "}
            </Icon>
            <Icon icon="instagram" size="3x">
              {" "}
            </Icon>
            <Icon icon="whatsapp" size="3x">
              {" "}
            </Icon>
          </Col>
        </Row>
      </Grid>
    </footer>
  );
}

export default footer;
