import Head from "next/head";
import { Fragment } from "react";
import styles from "../../../styles/components/Lowerpage.module.css"

function LowerPage() {
  return (
    <Fragment>
      <Head>
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
          Crossorigin="anonymous"
        />
      </Head>
      <div className={styles.container}>
        <div className="container">
        <h2> Our Courses </h2>
        <p className="text-center lowerParagraph ">
          {" "}
          Comprehensive Online Learning Platform with Students-Centric Coaching
          for aspirants of <br/> government job exams in India. All our Courses are
          structured & aligned with exam syllabus to <br/> help you best prepare for
          it. Our Program is powered by India's Awarded Coaching Institute to <br/>
          Make Your DREAM come TRUE.
        </p>

        <div className={styles.lowerRow}>
          <div className="col-sm-4 card box">
            <img
              src="https://upgrad-images.s3-ap-southeast-1.amazonaws.com/images/courses/images_1615448786980_42%402x.jpg"
              className="card-img-top imgMain"
              alt="logo"
            />
            <div className="card-body">
            <h4 className="card-title">
                Banking and Insurance
              </h4>
              <p className="card-text cardPara " style={{ color: "#557577" }}>
                In India, banking exams are often considered to be in high
                regard for the candidates aspiring to step into the banking
                domain. Every year lakhs of aspirants appear for bank exams, but
                very handful of them clears it. Candidates can start their
                preparation for upcoming exams with this Comprehensive Learning
                Program .
              </p>
              <button href="#" className="btn btn-danger">
                KNOW MORE
              </button>
            </div>
          </div>
          <div className="col-sm-4 card box">
            <img
              src="https://upgrad-images.s3-ap-southeast-1.amazonaws.com/images/courses/images_1615448786980_42%402x.jpg"
              alt="logo"
              className="card-img-top"
            />
            <div className="card-body">
              <h4 className="card-title">
                Banking and Insurance
              </h4>
              <p className="card-text cardPara " style={{ color: "#557577" }}>
                In India, banking exams are often considered to be in high
                regard for the candidates aspiring to step into the banking
                domain. Every year lakhs of aspirants appear for bank exams, but
                very handful of them clears it. Candidates can start their
                preparation for upcoming exams with this Comprehensive Learning
                Program .
              </p>
              <button href="#" className="btn btn-danger">
                KNOW MORE
              </button>
            </div>
          </div>
          <div className="col-sm-4 card box">
            <img
              src="https://upgrad-images.s3-ap-southeast-1.amazonaws.com/images/courses/images_1615448786980_42%402x.jpg"
              alt="logo"
              className="card-img-top"
            />

            <div className="card-body">
            <h4 className="card-title">
                Banking and Insurance
              </h4>
              <p className="card-text cardPara " style={{ color: "#557577" }}>
                In India, banking exams are often considered to be in high
                regard for the candidates aspiring to step into the banking
                domain. Every year lakhs of aspirants appear for bank exams, but
                very handful of them clears it. Candidates can start their
                preparation for upcoming exams with this Comprehensive Learning
                Program .
              </p>
              <button href="#" className="btn btn-danger">
                KNOW MORE
              </button>
            </div>
          </div>
        </div>
        </div>
      </div>
    </Fragment>
  );
}

export default LowerPage;
