import styles from "../../../styles/components/Meganavbar.module.css"

function Meganavbar() {
    return (
      <div className={styles.main}>
        <a>
          FEATURES <span className="vl"></span>{" "}
        </a>
        <a>
          RESOURCES <span className="vl"></span>{" "}
        </a>
        <a>
          2020 ANSWER KEY <span className="vl"></span>{" "}
        </a>
        <a>
          BLOGS <span className="vl"></span>{" "}
        </a>
        <a>
          NOTIFICATIONS <span className="vl"></span>{" "}
        </a>
        <a>CURRENT AFFAIRS </a>
      </div>
    );
  }
  
  export default Meganavbar;