import styles from "../../../styles/components/Navbar.module.css";
import { Icon } from "rsuite";
import { useSelector } from "react-redux";
import Link from "next/link";
import { getCartItem } from "../../store/counterSlice";

function MainNavbar({ onSelect, activeKey, ...props }) {
  const cartItem = useSelector(getCartItem);
  return (
    <>
      <div className={styles.navMain}>
        <img
          src="https://website.upgrad.bitelit.com/assets/images/uglogo.svg"
          alt="logo"
          href="/"
        />
        <h6 className={styles.logo}> MY PROGRAM </h6>
        <ul>
          <li>
            <Link className="btn btn-outline-success" href="/cart/Cart">
              Cart
            </Link>{" "}
            {cartItem}
          </li>
          <li>
            <a>
              <Icon icon="bell" size="2x">
                {" "}
              </Icon>{" "}
            </a>
          </li>
          <li>
            <a href="/courses/course"> Courses </a>
          </li>
          <li>
            <a> Login </a>
          </li>
        </ul>
      </div>
    </>
  );
}

export default MainNavbar;
