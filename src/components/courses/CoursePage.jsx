import Head from "next/head";
import { Fragment } from "react";
import { useDispatch } from "react-redux";
import styles from "../../../styles/Courses/Course.module.css";
import CourseList from "../../courseList/CourseList";
import { addToCart } from "../../store/counterSlice";

function CoursePage() {
  const dispatch = useDispatch();
  return (
    <Fragment>
      <Head>
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
          Crossorigin="anonymous"
        />
      </Head>
      <div className={styles.container}>
        <div className="container">
          <h2> Our Courses For Banking </h2>
          <p className="text-center lowerParagraph ">
            Comprehensive Online Learning Platform with Students-Centric
            Coaching for aspirants of <br /> government job exams in India. All
            our Courses are structured & aligned with exam syllabus to <br />
            help you best prepare for it. Our Program is powered by India's
            Awarded Coaching Institute to <br />
            Make Your DREAM come TRUE.
          </p>

          <div className={styles.lowerRow}>
            {CourseList.map((course) => {
              return (
                <div className="card" key={course.id}>
                  <img
                    src={course.image}
                    className="card-img-top"
                    alt="course"
                  />
                  <div className="card-body">
                    <h4 className="card-title">{course.name}</h4>
                    <p className="card-text">{course.description}</p>
                    <hr />
                    <p className="card-text">
                      
                      Total Price:
                      <span>
                        
                        <b> ₹{course.price} </b> <del> ₹500 </del>
                      </span>
                    </p>

                    <button
                      href="#"
                      className="btn btn-danger"
                      onClick={() => dispatch(addToCart({ course: course }))}
                    >
                      ADD TO CART
                    </button>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </Fragment>
  );
}
export default CoursePage;
