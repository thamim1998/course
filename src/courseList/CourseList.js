const Course = [
  {
    name: "Introduction to Programming",
    image:"https://upgrad-images.s3-ap-southeast-1.amazonaws.com/images/courses/images_1615448786980_42%402x.jpg",
    price: 450,
    description:
      "In India, banking exams are often considered to be in high regard for the candidates aspiring to step into the banking domain.",
    id: 1,
  },
  {
    name: "React-Redux Toolkit",
    image:"https://upgrad-images.s3-ap-southeast-1.amazonaws.com/images/courses/images_1615448786980_42%402x.jpg",
    price: 450,
    description:
      "In India, banking exams are often considered to be in high regard for the candidates aspiring to step into the banking domain.",
    id: 2,

  },
  {
    name: "IBM Data Science",
    image:"https://upgrad-images.s3-ap-southeast-1.amazonaws.com/images/courses/images_1615448786980_42%402x.jpg",
    price: 450,
    description:
      "In India, banking exams are often considered to be in high regard for the candidates aspiring to step into the banking domain.",
    id: 3,
  }
];

export default Course;
