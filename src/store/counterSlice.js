import { createSelector, createSlice } from "@reduxjs/toolkit";
import Course from "../courseList/CourseList";

export const counterSlice = createSlice({
  name: "counter",
  initialState: {
    course: Course,
    cart: [],
    totalAmount:0,
  },
  reducers: {
    addToCart: (state, action) => {
      console.log(action.payload);
      state.cart.push(action.payload.course);
    },
    removeCart: (state, action) => {
      console.log("logging action", action.payload);
      console.log("state", state);
      state.cart = state.cart.filter((course) => course.id !== action.payload); // (course) = inside function iterator
      // course.map((course) => {
      //   state.cart.course.filter(
      //     (removeFromCart) => removeFromCart.course.id !== action.payload
      //   );
      // });
    },
    totalAmount: (state, action) => {
       let totalprice = 0;
      state.cart.forEach((course) => {
        totalprice += course.price;
       
        console.log("amount",totalprice)
      });
      state.totalAmount = totalprice;

    //  var amount= state.cart.reduce((accumulator,currentValue = course.price) =>{
    //      return totalprice =+ currentValue

    //   },totalprice )
    //   console.log(amount)
    //   state.totalAmount = totalprice
    },
 
  },
});

export const { addToCart, removeCart, totalAmount } = counterSlice.actions;

export default counterSlice.reducer;

//create selector and return cart (fetch data).

export const getCartItem = createSelector(
  (state) => state.counter.cart,
  (cart) => cart.length
);

export const getCarts = createSelector(
  (state) => state.counter.cart,
  (cart) => cart
);

export const getTotal = createSelector(
  (state)=> state.counter,
  (counter)=> counter.totalAmount,
)